// setup express
const express = require('express');
const app = express();
const port = 5000;
app.use(express.json());

// setup bcrypt
const bcrypt = require('bcrypt');
const saltRounds = 10;

// global var to save hashed password
var storedHash;

// sample text to apply hashing
const sampleText = 'ReskillAmericans123';

// route 
app.post('/check', checkPassword);

// encrypt sample text and save in global var storedHash
bcrypt.hash(sampleText, saltRounds)
    .then((hash) => {
        storedHash = hash;
    })
    .catch((err) => {
        throw err;
    });

// function to check password against stored hash
// and return result to client
function checkPassword(req, res) {
    if (!req.body.pass) return res.status(400).json({
        message: 'Bad request'
    });
    bcrypt.compare(req.body.pass, storedHash)
        .then((result) => {
            return res.status(200).json({
                result
            })
        })
        .catch((err) => {
            return res.status(500).json({
                message: 'Error in checking password'
            })
        })
}
// start server
app.listen(port, () => console.log(`app listening on port ${port}`));